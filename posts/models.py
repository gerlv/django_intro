from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=64)
    content = models.TextField()
    created_by = models.ForeignKey(User, on_delete='cascade')
